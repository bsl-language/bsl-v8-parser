import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.junit.jupiter.api.Test;

public class SimpleTest {

  @Test
  void test() {
    var lexer = new com.gitlab.eightm.BSLTokens(CharStreams.fromString(
        """
        Async Процедура Тест(await)
          Ждать = test
        КонецПроцедуры
        """
    ));
    var tokens = new CommonTokenStream(lexer);
    var parser = new com.gitlab.eightm.BSL(tokens);
    var tree = parser.module();
    System.out.println();
  }
}
