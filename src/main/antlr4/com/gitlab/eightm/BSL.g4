parser grammar BSL;

options {
    tokenVocab = BSLTokens;
}

@parser::members {
    boolean isAsyncMethod = false;
}

identifier: {!isAsyncMethod}? ASYNC
    | {!isAsyncMethod}? AWAIT
    | IDENTIFIER;

module: varPartition? methodPartition? mainProgramPartition? EOF;
varPartition: variableDeclaration+;
variableDeclaration: VAR identifier EXPORT? (COMMA identifier EXPORT?)* SEMICOLON;

mainProgramPartition: mainOperator+;

methodPartition: methodDeclaration+;
methodDeclaration: procedure | function;

procedure: ASYNC? { isAsyncMethod = _localctx.ASYNC() != null; } PROCEDURE identifier LPAREN methodArgs? RPAREN EXPORT?
    variableDeclaration*
    methodBody?
END_PROCEDURE { isAsyncMethod = false; };

function: ASYNC? { isAsyncMethod = _localctx.ASYNC() != null; } FUNCTION identifier LPAREN methodArgs? RPAREN EXPORT?
    variableDeclaration*
    methodBody?
END_FUNCTION { isAsyncMethod = false; };

methodArgs: methodArgument COMMA* (COMMA methodArgument)*;
methodArgument: VAL? identifier (defaultValue)?;

methodBody: operator+;

defaultValue: EQUAL literal;

// operators

mainOperator: (
    ifOperator
    | newOperator
    | addHandlerOperator
    | removeHandlerOperator
    | tryOperator
    | raiseOperator
    | forOperator
    | forEachOperator
    | whileOperator
    | assignOperator
    | dereference
    | methodCall);


operator: (mainOperator
    | breakOperator
    | continueOperator
    | returnOperator
    | awaitExpression) SEMICOLON?;


returnOperator: RETURN expression?;

newOperator: NEW identifier (LPAREN callArgs RPAREN)?
    | NEW LPAREN expression (COMMA expression)? RPAREN;

addHandlerOperator: ADD_HANDLER event COMMA eventHandler;
removeHandlerOperator: REMOVE_HANDLER event COMMA eventHandler;
event: expression DOT identifier;
eventHandler: expression DOT identifier;

tryOperator: TRY tryBody? EXCEPT exceptBody? ENDTRY;
tryBody: operator+;
exceptBody: operator+;

ifOperator: IF expression THEN
    ifBody?
    elsIfBranch*
    elseBranch?
    ENDIF;
elsIfBranch: ELSIF  expression THEN elsIfBody?;
elseBranch: ELSE elseBody?;

ifBody: operator+;
elsIfBody: operator+;
elseBody: operator+;

raiseOperator: RAISE expression | RAISE LPAREN expression
    (COMMA expression)? (COMMA expression)? (COMMA expression)? (COMMA expression)? RPAREN;

forOperator: FOR identifier EQUAL expression TO expression DO
    forBody?
    ENDDO;
forEachOperator: FOR EACH identifier IN expression DO
    forBody?
    ENDDO;
whileOperator: WHILE expression DO
    forBody?
    ENDDO;
forBody: operator+;
breakOperator: BREAK;
continueOperator: CONTINUE;

assignOperator: target EQUAL expression;
target: dereference | propertyAccess | identifier;

// expressions
expression: dereference
    | (MINUS | PLUS) expression
    | expression (ASTERISK | SLASH | PERCENT) expression
    | expression (PLUS | MINUS) expression
    | expression (LESS | LESS_OR_EQUAL | EQUAL | GREATER_OR_EQUAL | GREATER | NOT_EQUAL) expression
    | NOT expression
    | expression AND expression
    | expression OR expression
    | {isAsyncMethod}? awaitExpression
    | newOperator
    | literal
    | LPAREN expression RPAREN
    | object;

awaitContextExpression: dereference
    | LPAREN (MINUS | PLUS) expression RPAREN
    | LPAREN expression (ASTERISK | SLASH | PERCENT) expression RPAREN
    | LPAREN expression (PLUS | MINUS) expression RPAREN
    | LPAREN expression (LESS | LESS_OR_EQUAL | EQUAL | GREATER_OR_EQUAL | GREATER | NOT_EQUAL) expression RPAREN
    | LPAREN NOT expression RPAREN
    | LPAREN expression AND expression RPAREN
    | LPAREN expression OR expression RPAREN
    | LPAREN {isAsyncMethod}? awaitExpression RPAREN
    | newOperator
    | literal
    | object;

awaitExpression: AWAIT awaitContextExpression;
dereference: object DOT object (DOT object)*;
object: methodCall
    | propertyAccess
    | identifier;
methodCall: identifier LPAREN callArgs RPAREN;
callArgs: expression? COMMA* (COMMA expression)*;
propertyAccess: (methodCall | identifier) (LBRACKET expression RBRACKET)+;

// literals
literal: stringLiteral | dateLiteral | numberLiteral | UNDEFINED | NULL;
stringLiteral: string;
string: (STRING | multiLineString)+;
multiLineString: STRING_START (STRING_PART)* STRING_END;
dateLiteral: DATE;

numberLiteral: floatNumber | decimalNumber;
floatNumber: DIGIT+ DOT DIGIT+;
decimalNumber: DIGIT+;